package titles

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
)

//Retrieve send the titles of urls to an only read channel
func Retrieve(urls ...string) <-chan string {
	c := make(chan string)
	for _, url := range urls {
		go func(url string) {
			resp, _ := http.Get(url)
			html, _ := ioutil.ReadAll(resp.Body)
			r, _ := regexp.Compile("<title>(.*?)<\\/title>")

			regex := r.FindStringSubmatch(string(html))
			if len(regex) == 0 {
				c <- fmt.Sprintf("Not found tag <title> in %s", url)
			} else {
				c <- regex[1]
			}
		}(url)
	}

	return c
}